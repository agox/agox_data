Contains the database files for C24 with parallel tempering. 

- 1_worker.tar.gz: 1 worker, 2000 iterations 
- 2_worker.tar.gz: 2 workers, 1000 iterations pr. worker
- 4_worker.tar.gz: 4 workers, 500 iterations pr. worker
- 8_worker.tar.gz: 8 workers, 250 iterations pr. worker

The pickle of the log objects are contained here: 

- logs_1.tar.gz
- logs_2.tar.gz
- logs_4.tar.gz
- logs_8.tar.gz

The scripts for each are

- C24_PT_TW1.py
- C24_PT_TW2.py
- C24_PT_TW4.py
- C24_PT_TW8.py 

They assume indexing starting at 1, like: 

python C24_PT_TW4.py -i 1 

So searches started with [1, 2, 3, 4] will communicate, so will [5, 6, 7, 8] and so on. 

