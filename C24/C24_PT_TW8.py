import matplotlib
matplotlib.use('Agg')
import argparse
import numpy as np
import os


# Common AGOXs
from ase import Atoms
from agox import AGOX
from agox.modules.databases.database_concurrent import ConcurrentDatabase
from agox.modules.environments import Environment
from agox.modules.evaluators import LocalOptimizationEvaluator
from agox.modules.generators import RattleGenerator
from agox.modules.samplers.parallel_tempering_sampler import ParallelTemperingSampler
from ase.constraints import FixedPlane
from agox.modules.postprocessors import CenteringPostProcess

TOTAL_WORKERS = 8
FACTOR = 1.5
LOWEST_TEMP = 0.05
TEMPERATURES = [LOWEST_TEMP*FACTOR**power for power in range(TOTAL_WORKERS)]
SYNC_FREQUENCY = 10
SWAP_FREQUENCY = 10
N_ATOMS = 24
NUM_ITERATIONS = 2000 // TOTAL_WORKERS
N_RATTLE = 12
RATTLE_AMPLITUDE = 1

################################################################################################
# Input arguments
################################################################################################

parser = argparse.ArgumentParser()
parser.add_argument('-i', '--index', type=int, default=0)
args = parser.parse_args()    
run_idx = args.index

################################################################################################
# Calculator
################################################################################################

import os
from xtb.ase.calculator import XTB
calc = XTB(method="GFN2-xTB")

################################################################################################
# Environment Settings:
################################################################################################

FP = [FixedPlane(a, (0, 0, 1)) for a in range(N_ATOMS)]
cell_size = 15
confinement_size = 8
confinement_cell = np.eye(3) * confinement_size
confinement_corner = np.array([(cell_size-confinement_size)/2, (cell_size-confinement_size)/2, (cell_size-confinement_size)/2])
confinement_corner[2] = cell_size / 2
confinement_cell[2, 2] = 0

template = Atoms(cell=np.eye(3)*cell_size, pbc=False)
environment = Environment(template=template, symbols='C24', confinement_cell=confinement_cell, 
                        confinement_corner=confinement_corner, constraints=FP)

################################################################################################
# Database Settings:
################################################################################################

DATABASE_PATH = None # Set this!
database_index = (run_idx-1) // TOTAL_WORKERS + 1
worker_index = (run_idx-1) % TOTAL_WORKERS
db_path = DATABASE_PATH.format(database_index)
database = ConcurrentDatabase(filename=db_path, store_meta_information=True, write_frequency=1, worker_number=worker_index, 
                            total_workers=TOTAL_WORKERS, sync_frequency=SYNC_FREQUENCY, order=7)

################################################################################################
# Parallel Tempering Settings:
################################################################################################

sampler = ParallelTemperingSampler(temperatures=TEMPERATURES, database=database, order=6, 
                            swap_frequency=SWAP_FREQUENCY, swap_order=9)

rattle_generator = RattleGenerator(**environment.get_confinement(), environment=environment, sampler=sampler, 
                    n_rattle=N_RATTLE, rattle_amplitude=RATTLE_AMPLITUDE, dimensionality=2, order=2)

evaluator = LocalOptimizationEvaluator(calc, gets={'get_key':'candidates'}, optimizer_kwargs={'logfile':None}, 
            optimizer_run_kwargs={'fmax':0.2, 'steps':100}, fix_template=True, constraints=environment.get_constraints())

centering = CenteringPostProcess()

################################################################################################
# Let get the show running! 
################################################################################################

agox = AGOX(database, rattle_generator, sampler, evaluator, centering, seed=run_idx)

agox.run(N_iterations=NUM_ITERATIONS)
