import matplotlib
matplotlib.use('Agg')
import argparse
import numpy as np
import os

# Common AGOXs
from ase import Atoms
from agox import AGOX

from agox.modules.environments import Environment
from agox.modules.evaluators import LocalOptimizationEvaluator
from agox.modules.generators import RattleGenerator

from agox.modules.databases.concurrent_ordered import OrderedDatabase
from agox.modules.samplers.parallel_tempering_sampler import ParallelTemperingSampler

from ase.constraints import FixedPlane
from agox.modules.postprocessors.constraints.box_constraint import BoxConstraint

from agox.modules.postprocessors import WrapperPostprocess
from agox.modules.postprocessors import CenteringPostProcess
from agox.modules.postprocessors import RelaxPostprocess
from ase.optimize import BFGS

# Local GPR model
from agox.modules.models.descriptors.soap import SOAP
from sklearn.gaussian_process.kernels import RBF, ConstantKernel as C
from agox.modules.models.local_GPR.LSGPR_MBKMeans import LSGPRModelMBKMeans
from agox.modules.models.priors.repulsive import Repulsive

TOTAL_WORKERS = 4
FACTOR = 3.
LOWEST_TEMP = 0.05
TEMPERATURES = [LOWEST_TEMP*FACTOR**power for power in range(TOTAL_WORKERS)]
SYNC_FREQUENCY = 10
SWAP_FREQUENCY = 10
NUM_ITERATIONS = 2000 // TOTAL_WORKERS
N_RATTLE = 6
RATTLE_AMPLITUDE = 2
C1, C2 = 0.75, 1.15

################################################################################################
# Input arguments
################################################################################################

parser = argparse.ArgumentParser()
parser.add_argument('-i', '--index', type=int, default=0)
args = parser.parse_args()    
run_idx = args.index

################################################################################################
# Calculator
################################################################################################

from agox.modules.helpers.gpaw_io import GPAW_IO

calc = GPAW_IO(mode = 'PW(500)',
               basis = 'dzp',
               xc='PBE',
               gpts = 'h2gpts(0.2, t.get_cell(), idiv = 8)',
               occupations='FermiDirac(0.1)',
               maxiter='99',
               mixer='Mixer(nmaxold=5, beta=0.05, weight=75)',
               nbands='-20',
               txt='calc.txt',
               kpts='(2,2,2)',
               modules=['from gpaw import PW',
                        'from gpaw.utilities import h2gpts',
                        'from gpaw import FermiDirac, PoissonSolver, Mixer, extra_parameters',
                        "extra_parameters['blacs'] = True"])

################################################################################################
# Environment Settings:
################################################################################################
cell = np.array([[4.347893599931024, 0.039346861173483044, 0.015209231938617354],
                 [-2.139871418630065, 4.14945670507723, -0.09161086350102725],
                 [0.015209231938613699, -0.09700205925211548, 5.363603004157097]])

confinement_cell = cell
confinement_corner = np.array([0]*3)


template = Atoms('', cell=cell, pbc=True)

environment = Environment(template=template, symbols='B12',
                          confinement_cell=confinement_cell,
                          confinement_corner=confinement_corner)


################################################################################################
# Database Settings:
################################################################################################

main_directory = '/home/roenne/papers/local-model/B12/data/'
sub_directory = os.path.splitext(os.path.basename(__file__))[0] + '/'
database_index = run_idx // TOTAL_WORKERS
worker_index = run_idx % TOTAL_WORKERS
db_path = main_directory + sub_directory + 'con_db{}.db'.format(database_index)
database = OrderedDatabase(filename=db_path, store_meta_information=True, write_frequency=1, worker_number=worker_index, 
                            total_workers=TOTAL_WORKERS, sync_frequency=SYNC_FREQUENCY, order=7)

################################################################################################
# Model settings:
################################################################################################

descriptor = SOAP(['B'], r_cut=4., nmax=4, lmax=3, sigma=1., weight=True, periodic=True)
kernel = C(1)*RBF(length_scale=20)
model = LSGPRModelMBKMeans(database=database, kernel=kernel, descriptor=descriptor, noise=0.01, prior=Repulsive(ratio=C1),
                           iteration_start_training=4, verbose=True)

################################################################################################
# Parallel Tempering Settings:
################################################################################################

sampler = ParallelTemperingSampler(temperatures=TEMPERATURES, database=database, order=6, 
                                   swap_frequency=SWAP_FREQUENCY, swap_order=9, use_counter=False)


rattle_generator = RattleGenerator(**environment.get_confinement(), c1=C1, c2=C2,
                                   environment=environment, sampler=sampler, n_rattle=N_RATTLE,
                                   rattle_amplitude=RATTLE_AMPLITUDE, dimensionality=3, order=2)

relaxer = RelaxPostprocess(model=model,
                           start_relax=8, 
                           optimizer_run_kwargs={'fmax':0.1, 'steps':200},
                           optimizer_kwargs ={'logfile': None}, 
                           optimizer=BFGS, order=2.5)


evaluator = LocalOptimizationEvaluator(calc, optimizer_kwargs={'logfile':None}, 
                                       optimizer_run_kwargs={'fmax':0.05, 'steps':1},
                                       gets={'get_key':'candidates'},
                                       verbose=True, use_counter=False)

center = CenteringPostProcess(order=2.6)
wrapper = WrapperPostprocess(order=2.7)

################################################################################################
# Let get the show running! 
################################################################################################

agox = AGOX(database, rattle_generator, relaxer, sampler, evaluator, center, wrapper, seed=run_idx)

agox.run(N_iterations=NUM_ITERATIONS)
