import matplotlib
matplotlib.use('Agg')
import argparse
import numpy as np
import os

# Common AGOXs
from ase import Atoms
from agox import AGOX

from agox.modules.environments import Environment
from agox.modules.evaluators import LocalOptimizationEvaluator
from agox.modules.generators import RattleGenerator

from agox.modules.databases.concurrent_ordered import OrderedDatabase
from agox.modules.samplers.parallel_tempering_sampler import ParallelTemperingSampler
from ase.constraints import FixedPlane
from agox.modules.postprocessors import CenteringPostProcess

from agox.modules.postprocessors import RelaxPostprocess
from ase.optimize import BFGS

# Local GPR model
from agox.modules.models.descriptors.soap import SOAP
from sklearn.gaussian_process.kernels import RBF, ConstantKernel as C
from agox.modules.models.local_GPR.LSGPR_MBKMeans import LSGPRModelMBKMeans
from agox.modules.models.priors.repulsive import Repulsive

TOTAL_WORKERS = 4
FACTOR = 3
LOWEST_TEMP = 0.05
TEMPERATURES = [LOWEST_TEMP*FACTOR**power for power in range(TOTAL_WORKERS)]
#HIGHEST_TEMP = 1
#TEMPERATURES = [HIGHEST_TEMP*FACTOR**(power - (TOTAL_WORKERS - 1)) for power in range(TOTAL_WORKERS)]
SYNC_FREQUENCY = 10
SWAP_FREQUENCY = 10
N_ATOMS = 20
NUM_ITERATIONS = 3000 // TOTAL_WORKERS
N_RATTLE = 6
RATTLE_AMPLITUDE = 3

################################################################################################
# Input arguments
################################################################################################

parser = argparse.ArgumentParser()
parser.add_argument('-i', '--index', type=int, default=0)
args = parser.parse_args()    
run_idx = args.index

################################################################################################
# Calculator
################################################################################################

#import os
#from xtb.ase.calculator import XTB
#calc = XTB(method="GFN2-xTB")

from agox.modules.helpers.gpaw_io import GPAW_IO

calc = GPAW_IO(mode='lcao',
               xc='PBE',
               basis='dzp',
               maxiter='200',
               kpts ='(1, 1, 1)',
               poissonsolver='PoissonSolver(eps=1e-7)',
               mixer='Mixer(0.05,5,100)',
               convergence="{'energy':0.005, 'density':1.0e-3, 'eigenstates':1.0e-3, 'bands':'occupied'}",
               occupations="FermiDirac(0.1)",
               gpts = "h2gpts(0.2, t.get_cell(), idiv = 8)",
               nbands='110%',
               txt='dft_log_lcao.txt', 
               modules=['from gpaw.utilities import h2gpts',
                        'from gpaw import FermiDirac',
                        'from gpaw.poisson import PoissonSolver',
                        'from gpaw import Mixer'])

#calc = GPAW_IO(poissonsolver = 'PoissonSolver(eps = 1.0e-7)',
#               mode = 'lcao',
#               basis = 'dzp',
#               xc='PBE',
#               gpts = 'h2gpts(0.2, t.get_cell(), idiv = 8)',
#               occupations='FermiDirac(0.1)',
#               maxiter='200',
#               mixer='Mixer(nmaxold=5, beta=0.05, weight=75)',
#               nbands='-50',
#               txt='None',
#               kpts='(1,1,1)',
#               modules=['from gpaw.utilities import h2gpts',
#                        'from gpaw import FermiDirac, PoissonSolver, Mixer, extra_parameters',
#                        "extra_parameters['blacs'] = True"])

################################################################################################
# Environment Settings:
################################################################################################

confinement_cell = np.array([[8, 0, 0], [0, 8, 0], [0, 0, 8]])
confinement_corner = np.array([3.5, 3.5, 3.5])
template = Atoms('', cell=[15,15,15], pbc=[0,0,0])

environment = Environment(template=template, symbols='Mg4Si4O12', confinement_cell=confinement_cell, 
                        confinement_corner=confinement_corner)

################################################################################################
# Database Settings:
################################################################################################

main_directory = '/home/ams/papers/local_model/silicate/pyroxene/n_4/data/'
sub_directory = os.path.splitext(os.path.basename(__file__))[0] + '/'
database_index = (run_idx) // TOTAL_WORKERS
worker_index = (run_idx) % TOTAL_WORKERS
db_path = main_directory + sub_directory + 'con_db{}.db'.format(database_index)
database = OrderedDatabase(filename=db_path, store_meta_information=True, write_frequency=1, worker_number=worker_index, 
                            total_workers=TOTAL_WORKERS, sync_frequency=SYNC_FREQUENCY, order=7)

################################################################################################
# Model settings:
################################################################################################

descriptor = SOAP(['O','Mg','Si'], r_cut=4.5, nmax=3, lmax=2, weight=True)
kernel = C(1)*RBF(length_scale=20)
model = LSGPRModelMBKMeans(database=database, kernel=kernel, descriptor=descriptor, noise=0.01,
                           prior=Repulsive(), iteration_start_training=4, verbose=True)

################################################################################################
# Parallel Tempering Settings:
################################################################################################

sampler = ParallelTemperingSampler(temperatures=TEMPERATURES, database=database, order=6, 
                                   swap_frequency=SWAP_FREQUENCY, swap_order=9)

rattle_generator = RattleGenerator(**environment.get_confinement(), environment=environment, sampler=sampler, 
                    n_rattle=N_RATTLE, rattle_amplitude=RATTLE_AMPLITUDE, order=2)

relaxer = RelaxPostprocess(model=model,
                           start_relax=5, 
                           optimizer_run_kwargs={'fmax':0.05, 'steps':40},
                           optimizer_kwargs ={'logfile': None}, 
                           optimizer=BFGS, constraints=environment.get_constraints(), order=2.5)


evaluator = LocalOptimizationEvaluator(calc, optimizer_kwargs={'logfile':None}, 
                                       optimizer_run_kwargs={'fmax':0.05, 'steps':3},
                                       gets={'get_key':'candidates'},
                                       constraints=environment.get_constraints(), 
                                       verbose=True)

centering = CenteringPostProcess()

################################################################################################
# Let get the show running! 
################################################################################################

agox = AGOX(database, rattle_generator, relaxer, sampler, evaluator, centering, seed=run_idx)

agox.run(N_iterations=NUM_ITERATIONS)
