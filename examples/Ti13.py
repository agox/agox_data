import matplotlib
matplotlib.use('Agg')
import argparse
import numpy as np
import os

# Common AGOXs
from ase import Atoms
from agox import AGOX
from agox.modules.helpers.gpaw_io import GPAW_IO

from agox.modules.environments import Environment
#from agox.modules.environments.environment_singular import EnvironmentSingular

from agox.modules.evaluators import LocalOptimizationEvaluator
from agox.modules.generators import RattleGenerator

from agox.modules.databases.concurrent_ordered import OrderedDatabase
from agox.modules.samplers.parallel_tempering_sampler import ParallelTemperingSampler
from agox.modules.postprocessors.constraints.box_constraint import BoxConstraint

from agox.modules.postprocessors import CenteringPostProcess

from agox.modules.postprocessors import RelaxPostprocess
from ase.optimize import BFGS

# Local GPR model
from agox.modules.models.descriptors.soap import SOAP
from sklearn.gaussian_process.kernels import RBF, ConstantKernel as C
from agox.modules.models.local_GPR.LSGPR_MBKMeans import LSGPRModelMBKMeans
from agox.modules.models.priors.repulsive import Repulsive
import random
TOTAL_WORKERS = 4
FACTOR = 3
LOWEST_TEMP = 0.05
TEMPERATURES = [LOWEST_TEMP*FACTOR**power for power in range(TOTAL_WORKERS)]
SYNC_FREQUENCY = 10
SWAP_FREQUENCY = 10
N_ATOMS = 24
NUM_ITERATIONS = 1000 // TOTAL_WORKERS
N_RATTLE = 12
RATTLE_AMPLITUDE = 1

################################################################################################
# Input arguments
################################################################################################

parser = argparse.ArgumentParser()
parser.add_argument('-i', '--index', type=int, default=0)
parser.add_argument('-d', '--data', type=int, default=0)

args = parser.parse_args()   
 
run_idx = args.index
database_index=args.data
#database_index=int(os.path.abspath(os.path.join(path, os.pardir)))
#database_index=int(os.path.basename(basename(path)))

################################################################################################
# Calculator
################################################################################################

#import os
#from xtb.ase.calculator import XTB
#calc = XTB(method="GFN2-xTB")
calc = GPAW_IO(mode='PW(400)',
            xc='PBE',
            setups="{'Ti': '4e'}",
            #basis='dzp',
            maxiter='150',
            kpts ='(1, 1, 1)',
            convergence="{'energy':0.0005, 'density':1.0e-3, 'eigenstates':1.0e-4, 'bands':'occupied'}",
            occupations="FermiDirac(0.1)",
            gpts = "h2gpts(0.2, t.get_cell(), idiv = 8)",
            nbands='110%',
            txt='dft_log_PW.txt',
            modules=['from gpaw.utilities import h2gpts', 'from gpaw import FermiDirac','from gpaw import setup_paths',
                     "setup_paths.insert(0, '/home/fbrix/gpaw_setups')" ])
'''
calc = GPAW_IO(poissonsolver = 'PoissonSolver(eps = 1.0e-7)',
               xc='PBE',
               gpts = 'h2gpts(0.2, t.get_cell(), idiv = 8)',
               occupations='FermiDirac(0.1)',
               setups="{'Ti': '4e'}",
               maxiter='99',
               mixer='Mixer(nmaxold=5, beta=0.05, weight=75)',
               nbands='-50',
               txt='dft_log_PW.txt',
               kpts='(1,1,1)',
               modules=['from gpaw.utilities import h2gpts',
                        'from gpaw import FermiDirac, PoissonSolver, Mixer, extra_parameters',
                        "extra_parameters['blacs'] = True"])
'''
################################################################################################
# Environment Settings:
################################################################################################

#FP = [FixedPlane(a, (0, 0, 1)) for a in range(N_ATOMS)]
cell_size = 15
confinement_size = 8
confinement_cell = np.eye(3) * confinement_size
confinement_corner = np.array([(cell_size-confinement_size)/2, (cell_size-confinement_size)/2, (cell_size-confinement_size)/2])
#confinement_corner[2] = cell_size / 2
#confinement_cell[2, 2] = 0

template = Atoms(cell=np.eye(3)*cell_size, pbc=False)

environment = Environment(template=template, symbols='Ti13', confinement_cell=confinement_cell, 
                        confinement_corner=confinement_corner)
BC = BoxConstraint(confinement_cell, confinement_corner, indices=environment.get_missing_indices())

################################################################################################
# Database Settings:
################################################################################################

main_directory = '/home/fbrix/Ti/benchmark/data/' #NB!
sub_directory = os.path.splitext(os.path.basename(__file__))[0] + '/'
#database_index = (run_idx) // TOTAL_WORKERS
worker_index = (run_idx) % TOTAL_WORKERS
db_path = main_directory + sub_directory + 'con_db{}.db'.format(database_index)
database = OrderedDatabase(filename=db_path, store_meta_information=True, write_frequency=1, worker_number=worker_index, 
                            total_workers=TOTAL_WORKERS, sync_frequency=SYNC_FREQUENCY, order=7)

################################################################################################
# Model settings:
################################################################################################

descriptor = SOAP(['Ti'], r_cut=4.5, nmax=3, lmax=2, sigma=0.5, weight=True)
kernel = C(1)*RBF(length_scale=20)
model = LSGPRModelMBKMeans(database=database, kernel=kernel, descriptor=descriptor, noise=0.01,
                           prior=Repulsive(ratio=0.85), iteration_start_training=4, verbose=True)

################################################################################################
# Parallel Tempering Settings:
################################################################################################

sampler = ParallelTemperingSampler(temperatures=TEMPERATURES, database=database, order=6, 
                                   swap_frequency=SWAP_FREQUENCY, swap_order=9)

rattle_generator = RattleGenerator(c1=0.85,c2=1.15,**environment.get_confinement(), environment=environment, sampler=sampler, 
                    n_rattle=N_RATTLE, rattle_amplitude=RATTLE_AMPLITUDE, dimensionality=3, order=2)

relaxer = RelaxPostprocess(model=model,
                           start_relax=5, 
                           optimizer_run_kwargs={'fmax':0.05, 'steps':40},
                           optimizer_kwargs ={'logfile': None}, 
                           optimizer=BFGS, constraints=environment.get_constraints(), order=2.5)


evaluator = LocalOptimizationEvaluator(calc, optimizer_kwargs={'logfile':None}, 
                                       optimizer_run_kwargs={'fmax':0.05, 'steps':1},
                                       gets={'get_key':'candidates'},
                                       constraints=environment.get_constraints(), 
                                       verbose=True)

centering = CenteringPostProcess()

################################################################################################
# Let get the show running! 
################################################################################################

agox = AGOX(database, rattle_generator, relaxer, sampler, evaluator, centering, seed=10*database_index+run_idx)

agox.run(N_iterations=NUM_ITERATIONS)
