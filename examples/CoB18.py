import matplotlib
matplotlib.use('Agg')
import argparse
import numpy as np
import os

# Common AGOXs
from ase import Atoms
from agox import AGOX

from agox.modules.environments import Environment
from agox.modules.evaluators import LocalOptimizationEvaluator
from agox.modules.generators import RattleGenerator

from agox.modules.databases.concurrent_ordered import OrderedDatabase
from agox.modules.samplers.parallel_tempering_sampler import ParallelTemperingSampler
from ase.constraints import FixedPlane, FixAtoms
from agox.modules.postprocessors import CenteringPostProcess

from agox.modules.postprocessors import RelaxPostprocess
from ase.optimize import BFGS

# Local GPR model
from agox.modules.models.descriptors.soap import SOAP
from sklearn.gaussian_process.kernels import RBF, ConstantKernel as C
from agox.modules.models.local_GPR.LSGPR_MBKMeans import LSGPRModelMBKMeans
from agox.modules.models.priors.repulsive import Repulsive

TOTAL_WORKERS = 10
FACTOR = 1.5
LOWEST_TEMP = 0.05
TEMPERATURES = [LOWEST_TEMP*FACTOR**power for power in range(TOTAL_WORKERS)]
SYNC_FREQUENCY = 10
SWAP_FREQUENCY = 10
N_ATOMS = 19
NUM_ITERATIONS = 2000 // TOTAL_WORKERS
N_RATTLE = 12
RATTLE_AMPLITUDE = 1

################################################################################################
# Input arguments
################################################################################################

parser = argparse.ArgumentParser()
parser.add_argument('-i', '--index', type=int, default=0)
args = parser.parse_args()    
run_idx = args.index

################################################################################################
# Calculator
################################################################################################

import os
from ase.calculators.orca import ORCA
n_cpu = 16
orca_input = 'PBE def2-SVP TightSCF'
orca_block = f'%scf maxiter 600 end \n%maxcore 1000 \n%pal nprocs {n_cpu} end'
calc = ORCA(label='orca',
		maxiter=600,
		charge=-1, mult=1, task='gradient',
		orcasimpleinput=orca_input,
		orcablocks=orca_block)

################################################################################################
# Environment Settings:
################################################################################################

FP = [FixedPlane(a, (0, 0, 1)) for a in range(N_ATOMS)]
cell_size = 15
confinement_size = 12
confinement_cell = np.eye(3) * confinement_size
confinement_corner = np.array([(cell_size-confinement_size)/2, (cell_size-confinement_size)/2, (cell_size-confinement_size)/2])
confinement_corner[2] = cell_size / 2
confinement_cell[2, 2] = 0

template = Atoms('Co', [[cell_size/2, cell_size/2, cell_size/2]], charges=[-1], cell=np.eye(3)*cell_size, pbc=False)
environment = Environment(template=template, symbols='B18', confinement_cell=confinement_cell, 
                        confinement_corner=confinement_corner, constraints=FP)

################################################################################################
# Database Settings:
################################################################################################

main_directory = '/home/tang/project/agox_local_model/CoB18/data/' #NB!
sub_directory = os.path.splitext(os.path.basename(__file__))[0] + '/'
database_index = (run_idx) // TOTAL_WORKERS
worker_index = (run_idx) % TOTAL_WORKERS
db_path = main_directory + sub_directory + 'con_db{}.db'.format(database_index)
print(db_path, flush=True)
database = OrderedDatabase(filename=db_path, store_meta_information=True, write_frequency=1, worker_number=worker_index, 
                            total_workers=TOTAL_WORKERS, sync_frequency=SYNC_FREQUENCY, order=7)

################################################################################################
# Model settings:
################################################################################################

descriptor = SOAP(['Co', 'B'], r_cut=5., nmax=4, lmax=3, sigma=0.5, weight=True)
kernel = C(1)*RBF(length_scale=20)
model = LSGPRModelMBKMeans(database=database, kernel=kernel, descriptor=descriptor, noise=0.01,
                           prior=Repulsive(ratio=0.85), iteration_start_training=4, verbose=True)

################################################################################################
# Parallel Tempering Settings:
################################################################################################

sampler = ParallelTemperingSampler(temperatures=TEMPERATURES, database=database, order=6, 
                                   swap_frequency=SWAP_FREQUENCY, swap_order=9)

rattle_generator = RattleGenerator(**environment.get_confinement(), environment=environment, sampler=sampler, 
                    n_rattle=N_RATTLE, rattle_amplitude=RATTLE_AMPLITUDE, dimensionality=2, order=2)

relaxer = RelaxPostprocess(model=model,
                           start_relax=5, 
                           optimizer_run_kwargs={'fmax':0.05, 'steps':40},
                           optimizer_kwargs ={'logfile': None}, 
                           optimizer=BFGS, constraints=environment.get_constraints(), order=2.5)

charges = np.zeros(N_ATOMS)
spins = np.zeros(N_ATOMS)
charges[0] = -1
spins[0] = 0
evaluator = LocalOptimizationEvaluator(calc, optimizer_kwargs={'logfile':None}, 
                                       atoms_kwargs={'charges':charges, 'spins':spins},
                                       optimizer_run_kwargs={'fmax':0.05, 'steps':10},
                                       gets={'get_key':'candidates'},
                                       constraints=environment.get_constraints(), 
                                       verbose=True)

centering = CenteringPostProcess()

################################################################################################
# Let get the show running! 
################################################################################################

agox = AGOX(database, rattle_generator, relaxer, sampler, evaluator, centering, seed=run_idx)

agox.run(N_iterations=NUM_ITERATIONS)