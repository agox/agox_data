import matplotlib
matplotlib.use('Agg')
import argparse
import numpy as np
import os

# Common AGOXs
from ase import Atoms
from agox import AGOX
from ase.io import read

from agox.modules.environments import Environment
from agox.modules.evaluators import LocalOptimizationEvaluator
from agox.modules.generators import RattleGenerator

from agox.modules.databases.concurrent_ordered import OrderedDatabase
from agox.modules.samplers.parallel_tempering_sampler import ParallelTemperingSampler
from ase.constraints import FixedPlane
from agox.modules.postprocessors import WrapperPostprocess

from agox.modules.postprocessors import RelaxPostprocess
from ase.optimize import BFGS

# Local GPR model
from agox.modules.models.descriptors.soap import SOAP
from sklearn.gaussian_process.kernels import RBF, ConstantKernel as C
from agox.modules.models.local_GPR.LSGPR_MBKMeans import LSGPRModelMBKMeans
from agox.modules.models.priors.repulsive import Repulsive
from agox.modules.models.sparsifiers.latest import LatestSparsifier

TOTAL_WORKERS = 4
LOWEST_TEMP = 0.05
HIGHEST_TEMP = 1
FACTOR = np.exp(1/(TOTAL_WORKERS - 1) * np.log(HIGHEST_TEMP/LOWEST_TEMP))
TEMPERATURES = [LOWEST_TEMP*FACTOR**power for power in range(TOTAL_WORKERS)]
SYNC_FREQUENCY = 10
SWAP_FREQUENCY = 10
NUM_ITERATIONS = 4000 // TOTAL_WORKERS
N_RATTLE = 3
RATTLE_AMPLITUDE = 3

################################################################################################
# Input arguments
################################################################################################

parser = argparse.ArgumentParser()
parser.add_argument('-i', '--index', type=int, default=0)
args = parser.parse_args()    
run_idx = args.index

################################################################################################
# Calculator
################################################################################################

from agox.modules.helpers.gpaw_io import GPAW_IO

calc = GPAW_IO(mode='PW(300)',
               xc='PBE',
               basis='dzp',
               maxiter='200',
               kpts ='(1, 1, 1)',
               convergence="{'energy':0.005, 'density':1.0e-4, 'eigenstates':1.0e-4, 'bands':'occupied'}",
               occupations="FermiDirac(0.1)",
               gpts = "h2gpts(0.2, t.get_cell(), idiv = 8)",
               nbands='110%',
               txt='dft_log_lcao.txt', 
               modules=['from gpaw.utilities import h2gpts',
                        'from gpaw import FermiDirac',
                    ])

################################################################################################
# Environment Settings:
################################################################################################

template = read('/home/hammer/agox2022jun/runs/znocu10/template/template_4x2_2lay_flip.traj',index='-1')

confinement_corner = np.array([2, 3, 4])
confinement_cell = template.get_cell() - np.eye(3) * 2 * confinement_corner
confinement_cell[2][2] = 4

N_Cu = 10

environment = Environment(template=template, symbols=f'Cu{N_Cu}', confinement_cell=confinement_cell, 
                        confinement_corner=confinement_corner)

################################################################################################
# Database Settings:
################################################################################################

main_directory = '/home/hammer/agox2022jun/runs/znocu10/data/'
sub_directory = os.path.splitext(os.path.basename(__file__))[0] + '/'
database_index = run_idx // TOTAL_WORKERS
worker_index = run_idx % TOTAL_WORKERS
db_path = main_directory + sub_directory + 'con_db{}.db'.format(database_index)
database = OrderedDatabase(filename=db_path, store_meta_information=True, write_frequency=1, worker_number=worker_index, 
                            total_workers=TOTAL_WORKERS, sync_frequency=SYNC_FREQUENCY, order=7, use_counter=False)

################################################################################################
# Model settings:
################################################################################################

descriptor = SOAP(environment.get_all_species(), r_cut=5, nmax=3, lmax=2, sigma=1, weight=True, periodic=True)
kernel = C(1)*RBF(length_scale=20)
sparsifier = LatestSparsifier(n_latest=160)
model = LSGPRModelMBKMeans(database=database, kernel=kernel, descriptor=descriptor, noise=0.01, prior=Repulsive(),
                           iteration_start_training=0, sparsifier=sparsifier, verbose=True)

################################################################################################
# Parallel Tempering Settings:
################################################################################################

sampler = ParallelTemperingSampler(temperatures=TEMPERATURES, database=database, order=6, 
                                   swap_frequency=SWAP_FREQUENCY, swap_order=9, use_counter=False)

rattle_generator = RattleGenerator(**environment.get_confinement(), environment=environment, sampler=sampler, 
                                   n_rattle=N_RATTLE, rattle_amplitude=RATTLE_AMPLITUDE, order=2, c1=0.7, c2=3)

relaxer = RelaxPostprocess(model=model,
                           start_relax=1, 
                           optimizer_run_kwargs={'fmax':0.05, 'steps':100},
                           optimizer_kwargs ={'logfile': '-'}, 
                           optimizer=BFGS, constraints=environment.get_constraints(), order=2.5)


evaluator = LocalOptimizationEvaluator(calc, optimizer_kwargs={'logfile':'-'}, 
                                       optimizer_run_kwargs={'fmax':0.05, 'steps':1},
                                       gets={'get_key':'candidates'},
                                       constraints=environment.get_constraints(), 
                                       verbose=True, use_counter=False)

wrapper = WrapperPostprocess()

################################################################################################
# Let get the show running! 
################################################################################################

agox = AGOX(database, rattle_generator, relaxer, sampler, evaluator, wrapper, seed=run_idx)

agox.run(N_iterations=NUM_ITERATIONS)
