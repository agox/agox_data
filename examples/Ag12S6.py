import matplotlib
matplotlib.use('Agg')
import argparse
import numpy as np
import os

# Common AGOXs
from ase import Atoms
from agox import AGOX

from agox.modules.environments import Environment
from agox.modules.evaluators import LocalOptimizationEvaluator
from agox.modules.generators import RattleGenerator

from agox.modules.databases.concurrent_ordered import OrderedDatabase
from agox.modules.samplers.parallel_tempering_sampler import ParallelTemperingSampler
from ase.constraints import FixedPlane
from agox.modules.postprocessors import CenteringPostProcess

from agox.modules.postprocessors import RelaxPostprocess
from ase.optimize import BFGS

# Local GPR model
from agox.modules.models.descriptors.soap import SOAP
from sklearn.gaussian_process.kernels import RBF, ConstantKernel as C
from agox.modules.models.local_GPR.LSGPR_MBKMeans import LSGPRModelMBKMeans
from agox.modules.models.priors.repulsive import Repulsive

TOTAL_WORKERS = 4
FACTOR = 1.5
LOWEST_TEMP = 0.1
TEMPERATURES = [LOWEST_TEMP*FACTOR**power for power in range(TOTAL_WORKERS)]
SYNC_FREQUENCY = 10
SWAP_FREQUENCY = 10
NUM_ITERATIONS = 1000 // TOTAL_WORKERS
N_RATTLE = 9
RATTLE_AMPLITUDE = 1

################################################################################################
# Input arguments
################################################################################################

parser = argparse.ArgumentParser()
parser.add_argument('-i', '--index', type=int, default=0)
args = parser.parse_args()    
run_idx = args.index

################################################################################################
# Calculator
################################################################################################

from agox.modules.helpers.gpaw_io import GPAW_IO

convergence = "{'energy': 0.005, 'density': 1.0e-4, 'eigenstates': 1.0e-4, 'bands': 'occupied'}"

calc = GPAW_IO(mode='lcao', basis = 'dzp', xc='PBE',
            kpts='(1,1,1)', txt='dft_log.txt', maxiter='150',
            convergence=convergence,
            occupations="{'name':'fermi-dirac', 'width':0.2}",
            gpts = 'h2gpts(0.2, t.get_cell(), idiv = 8)',
            modules=['from gpaw.utilities import h2gpts', 'from gpaw import extra_parameters',
            "extra_parameters['blacs'] = True"])

################################################################################################
# Environment Settings:
################################################################################################

cell_size = 15
confinement_size = 8

confinement_cell = np.eye(3) * confinement_size
confinement_corner = np.ones(3) * (cell_size/2 - confinement_size/2)

template = Atoms(cell=np.eye(3)*cell_size, pbc=False)
environment = Environment(template=template, symbols='Ag12S6', confinement_cell=confinement_cell, 
                        confinement_corner=confinement_corner)

################################################################################################
# Database Settings:
################################################################################################

main_directory = '/home/machri/Projects/agox/data/local_model/'
sub_directory = os.path.splitext(os.path.basename(__file__))[0] + '/'
database_index = (run_idx-1) // TOTAL_WORKERS
worker_index = (run_idx-1) % TOTAL_WORKERS
db_path = main_directory + sub_directory + 'con_db{}.db'.format(database_index)

database = OrderedDatabase(filename=db_path, store_meta_information=True, write_frequency=1, worker_number=worker_index, 
                            total_workers=TOTAL_WORKERS, sync_frequency=SYNC_FREQUENCY, order=7)


print('Worker index: {}'.format(worker_index))
print('Database index: {}'.format(database_index))

################################################################################################
# Model settings:
################################################################################################

descriptor = SOAP(['Ag', 'S'], r_cut=4.5, nmax=3, lmax=2, sigma=0.5, weight=True, periodic=False)
kernel = C(1)*RBF(length_scale=20)
model = LSGPRModelMBKMeans(database=database, kernel=kernel, descriptor=descriptor, noise=0.01,
                           prior=Repulsive(ratio=0.85), iteration_start_training=4, verbose=True)

################################################################################################
# Parallel Tempering Settings:
################################################################################################

sampler = ParallelTemperingSampler(temperatures=TEMPERATURES, database=database, order=6, 
                                   swap_frequency=SWAP_FREQUENCY, swap_order=9)

rattle_generator = RattleGenerator(**environment.get_confinement(), environment=environment, sampler=sampler, 
                    n_rattle=N_RATTLE, rattle_amplitude=RATTLE_AMPLITUDE, dimensionality=3, order=2, 
                    c1=.85, c2=1.15)

relaxer = RelaxPostprocess(model=model,
                           start_relax=5, 
                           optimizer_run_kwargs={'fmax':0.05, 'steps':40},
                           optimizer_kwargs ={'logfile': None}, 
                           optimizer=BFGS, constraints=environment.get_constraints(), order=2.5)


evaluator = LocalOptimizationEvaluator(calc, optimizer_kwargs={'logfile':None}, 
                                       optimizer_run_kwargs={'fmax':0.05, 'steps':2},
                                       gets={'get_key':'candidates'},
                                       constraints=environment.get_constraints(), 
                                       verbose=True)

centering = CenteringPostProcess()

################################################################################################
# Let get the show running! 
################################################################################################

agox = AGOX(database, rattle_generator, relaxer, sampler, evaluator, centering, seed=run_idx)

agox.run(N_iterations=NUM_ITERATIONS)