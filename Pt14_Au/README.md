Contains the database files for Pt14 on gold. 

- bh_pt14.tar.gz: Basin-hopping. 
- ea_pt14.tar.gz: Evolutionary algorithm. 
- gofee_pt14.tar.gz: GOFEE
- rss_pt14.tar.gz: Random structure search. 

For EA, BH, RSS the tarballs also contain a numpy array of dimension (number_of_restarts, number_of_iterations) that 
specifies how many single-point calculations the algorithm has used up to that iteration cumulatively. 
This is omitted for GOFEE because it uses 1 pr. iteration. 

Additional trajectory files contain the structures extracted using the spectral graph analysis scheme explained in the paper: 

- all_structures.traj: All structures extracted from the GOFEE search. 
- unique_structures_graph_clustered.traj: Unique structures extracted using the graph technique. 
- unique_structures_relaxed_graph_clustered.traj: Unique structures extracted using the graph technique after local optimization.