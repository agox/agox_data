from os import replace
from random import Random
import matplotlib

from agox.modules.generators.random import RandomGenerator
matplotlib.use('Agg')
import argparse
import numpy as np
from ase.build import fcc100 

# Common AGOXs
from agox import AGOX
from agox.modules.databases import Database
from agox.modules.environments import Environment
from agox.modules.evaluators import LocalOptimizationEvaluator
from agox.modules.generators import RattleGenerator
from agox.modules.samplers import GeneticSampler, DistanceComparator
from agox.modules.collectors import StandardCollector

NUM_EPISODES = 1250

################################################################################################
# Input arguments
################################################################################################

parser = argparse.ArgumentParser()
parser.add_argument('-i', '--index', type=int, default=0)
parser.add_argument('-t', '--task_per_node', type=int, default=1)
args = parser.parse_args()    
run_idx = args.index

################################################################################################
# Calculator
################################################################################################

from ase.calculators.emt import EMT
calc = EMT()

################################################################################################
# General settings:
################################################################################################

template = fcc100('Au', (6, 6, 3), vacuum=10)

confinement_cell = np.array([[12.1169818, 0, 0], [0, 12.1169818, 0], [0, 0, 10]])
confinement_corner = np.array([2.5964961, 2.5964961, 13.08])

environment = Environment(template=template, symbols='Pt14', confinement_cell=confinement_cell, 
                        confinement_corner=confinement_corner)

# Database
db_path = 'db{}.db'.format(run_idx)
database = Database(filename=db_path, order=3)

################################################################################################
# Generator settings:
################################################################################################

from agox.modules.models.descriptors.simple_fingerprint import SimpleFingerprint

descriptor = SimpleFingerprint(species=['Pt', 'Au'])
comparator = DistanceComparator(descriptor, threshold=0.5)

sampler = GeneticSampler(database=database, comparator=comparator, population_size=10, order=4)

random_generator = RandomGenerator(**environment.get_confinement(), environment=environment, sampler=None, may_nucleate_at_several_places=True, use_mic=False)
rattle_generator = RattleGenerator(**environment.get_confinement(), environment=environment, sampler=sampler, use_mic=False)

generators = [random_generator, rattle_generator]

num_candidates = {0:[sampler.population_size, 0], 2:[0, sampler.population_size]}

collector = StandardCollector(generators=generators, sampler=sampler, environment=environment, 
                            num_candidates=num_candidates, order=1)

evaluator = LocalOptimizationEvaluator(calc, gets={'get_key':'candidates'}, optimizer_kwargs={'logfile':None},
                                optimizer_run_kwargs={'fmax':0.05, 'steps':400}, constraints=environment.get_constraints(), 
                                number_to_evaluate=sampler.population_size, use_all_traj_info=False, order=2)

################################################################################################
# Let get the show running! 
################################################################################################

agox = AGOX(database, sampler, collector, evaluator, seed=run_idx)

agox.run(N_iterations=50)

