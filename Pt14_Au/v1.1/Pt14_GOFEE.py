from os import replace
import matplotlib
matplotlib.use('Agg')
import argparse
import numpy as np
from ase.io import read, write
from ase.build import fcc100 

# Common AGOXs
from agox import AGOX
from agox.modules.databases import Database
from agox.modules.environments import Environment
from agox.modules.acquisitors import LowerConfidenceBoundAcquisitor
from agox.modules.models import ModelGPR
from agox.modules.evaluators import SinglePointEvaluator
from agox.modules.collectors import StandardCollector
from agox.modules.generators import RattleGenerator, RandomGenerator, CenterOfGeometryGenerator
from agox.modules.postprocessors import MPIRelaxPostprocess
from agox.modules.samplers import KMeansSampler

KAPPA = 2
NUM_EPISODES = 1000

################################################################################################
# Input arguments
################################################################################################

parser = argparse.ArgumentParser()
parser.add_argument('-i', '--index', type=int, default=0)
parser.add_argument('-t', '--task_per_node', type=int, default=1)
args = parser.parse_args()    
run_idx = args.index

################################################################################################
# Calculator
################################################################################################

from ase.calculators.emt import EMT
calc = EMT()

evaluator = SinglePointEvaluator(calc)

################################################################################################
# General settings:
################################################################################################

template = fcc100('Au', (6, 6, 3), vacuum=10)

confinement_cell = np.array([[12.1169818, 0, 0], [0, 12.1169818, 0], [0, 0, 10]])
confinement_corner = np.array([2.5964961, 2.5964961, 13.08])

environment = Environment(template=template, symbols='Pt14', confinement_cell=confinement_cell, 
                        confinement_corner=confinement_corner)

# Database
db_path = 'db{}.db'.format(run_idx)
database = Database(filename=db_path)

model = ModelGPR.default(environment, database)
acquisitor = LowerConfidenceBoundAcquisitor(model, kappa=KAPPA, verbose=True)

# ################################################################################################
# # Generator settings:
# ################################################################################################

random_generator = RandomGenerator(**environment.get_confinement(), may_nucleate_at_several_places=True, use_mic=False)
rattle_generator = RattleGenerator(**environment.get_confinement(), use_mic=False)
cog_generator = CenterOfGeometryGenerator(**environment.get_confinement(), use_mic=False)

generators = [random_generator, rattle_generator, cog_generator]
num_candidates = [10, 15, 5]
               
# # ################################################################################################
# # # Ensemble / Sampler / Acquisitor
# # ################################################################################################

relaxer = MPIRelaxPostprocess(model=acquisitor.get_acquisition_calculator(database), start_relax=10, sleep_timing=0.2, 
                                        database=database, optimizer_run_kwargs={'fmax':0.2, 'steps':50}, 
                                        constraints=environment.get_constraints())
                                        

sampler = KMeansSampler(model.model.featureCalculator, database=database, sample_size=10, max_energy=25, use_saved_features=True)

collector = StandardCollector(generators=generators, sampler=sampler, environment=environment, num_candidates=num_candidates)

# ################################################################################################
# # Let get the show running! 
# ################################################################################################

agox = AGOX(environment, database, collector, sampler, acquisitor, relaxer, evaluator, seed=run_idx)

agox.run(N_iterations=1000)

