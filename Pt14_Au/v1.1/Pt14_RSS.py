from os import replace
import matplotlib
matplotlib.use('Agg')
import argparse
import numpy as np
from ase.io import read, write
from ase.build import fcc100 

# Common AGOXs
from agox import AGOX
from agox.modules.databases import Database
from agox.modules.environments import Environment
from agox.modules.evaluators import LocalOptimizationEvaluator
from agox.modules.generators import RandomGenerator

NUM_EPISODES = 1000

################################################################################################
# Input arguments
################################################################################################

parser = argparse.ArgumentParser()
parser.add_argument('-i', '--index', type=int, default=0)
parser.add_argument('-t', '--task_per_node', type=int, default=1)
args = parser.parse_args()    
run_idx = args.index

################################################################################################
# Calculator
################################################################################################

from ase.calculators.emt import EMT
calc = EMT()

################################################################################################
# General settings:
################################################################################################

template = fcc100('Au', (6, 6, 3), vacuum=10)

confinement_cell = np.array([[12.1169818, 0, 0], [0, 12.1169818, 0], [0, 0, 10]])
confinement_corner = np.array([2.5964961, 2.5964961, 13.08])

environment = Environment(template=template, symbols='Pt14', confinement_cell=confinement_cell, 
                        confinement_corner=confinement_corner)

# Database
db_path = 'db{}.db'.format(run_idx)
database = Database(filename=db_path, order=3)

evaluator = LocalOptimizationEvaluator(calc, gets={'get_key':'candidates'}, optimizer_kwargs={'logfile':None}, 
                                optimizer_run_kwargs={'fmax':0.05, 'steps':400}, constraints=environment.get_constraints(), 
                                use_all_traj_info=False, order=2)

################################################################################################
# Generator settings:
################################################################################################

random_generator = RandomGenerator(**environment.get_confinement(), environment=environment, sampler=None, 
                                may_nucleate_at_several_places=True, use_mic=False, order=1)

# ################################################################################################
# # Let get the show running! 
# ################################################################################################

agox = AGOX(database, random_generator, evaluator, seed=run_idx)

agox.run(N_iterations=500)

