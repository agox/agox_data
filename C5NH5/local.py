import matplotlib
matplotlib.use('Agg')
import argparse
import numpy as np
import os

# Common AGOXs
from ase import Atoms
from agox import AGOX

from agox.modules.environments import Environment
from agox.modules.evaluators import LocalOptimizationEvaluator
from agox.modules.generators import RattleGenerator

from agox.modules.databases.concurrent_ordered import OrderedDatabase
from agox.modules.samplers.parallel_tempering_sampler import ParallelTemperingSampler

from ase.constraints import FixedPlane
from agox.modules.postprocessors.constraints.box_constraint import BoxConstraint

from agox.modules.postprocessors import CenteringPostProcess
# from agox.modules.postprocessors.postprocess_minimum_dist import MinimumDistPostProcess #should probably add later
from agox.modules.postprocessors import RelaxPostprocess
from ase.optimize import BFGS

# Local GPR model
from agox.modules.models.descriptors.soap import SOAP
from sklearn.gaussian_process.kernels import RBF, ConstantKernel as C
from agox.modules.models.local_GPR.LSGPR_MBKMeans import LSGPRModelMBKMeans
from agox.modules.models.priors.repulsive import Repulsive

TOTAL_WORKERS = 4
FACTOR = 1.5
LOWEST_TEMP = 0.05
TEMPERATURES = [LOWEST_TEMP*FACTOR**power for power in range(TOTAL_WORKERS)]
SYNC_FREQUENCY = 10
SWAP_FREQUENCY = 10
N_ATOMS = 11
NUM_ITERATIONS = 2000 // TOTAL_WORKERS
N_RATTLE = 3
RATTLE_AMPLITUDE = 2

################################################################################################
# Input arguments
################################################################################################

parser = argparse.ArgumentParser()
parser.add_argument('-i', '--index', type=int, default=0)
args = parser.parse_args()    
run_idx = args.index

################################################################################################
# Calculator
################################################################################################

from agox.modules.helpers.gpaw_io import GPAW_IO

calc = GPAW_IO(poissonsolver = 'PoissonSolver(eps = 1.0e-7)',
               mode = 'lcao',
               basis = 'dzp',
               xc='PBE',
               gpts = 'h2gpts(0.2, t.get_cell(), idiv = 8)',
               occupations='FermiDirac(0.1)',
               maxiter='99',
               mixer='Mixer(nmaxold=5, beta=0.05, weight=75)',
               nbands='-50',
               txt='None',
               kpts='(1,1,1)',
               modules=['from gpaw.utilities import h2gpts',
                        'from gpaw import FermiDirac, PoissonSolver, Mixer, extra_parameters',
                        "extra_parameters['blacs'] = True"])

################################################################################################
# Environment Settings:
################################################################################################

xy_constraints = [FixedPlane(i, (0,0,1)) for i in range(N_ATOMS)]

confinement_corner = np.array([3.5, 3.5, 7])
confinement_cell = np.diag([5, 5, 0])
BC = BoxConstraint(confinement_cell, confinement_corner, indices=list(range(N_ATOMS)))
constraints = [BC] + xy_constraints

template = Atoms('', cell=[12,12,12], pbc=False)
environment = Environment(template=template, symbols='H5C5N', constraints=constraints,
                                  confinement_cell=confinement_cell, confinement_corner=confinement_corner)


################################################################################################
# Database Settings:
################################################################################################

main_directory = '/home/roenne/papers/local-model/C5NH5/data/'
sub_directory = os.path.splitext(os.path.basename(__file__))[0] + '/'
database_index = run_idx // TOTAL_WORKERS
worker_index = run_idx % TOTAL_WORKERS
db_path = main_directory + sub_directory + 'con_db{}.db'.format(database_index)
database = OrderedDatabase(filename=db_path, store_meta_information=True, write_frequency=1, worker_number=worker_index, 
                            total_workers=TOTAL_WORKERS, sync_frequency=SYNC_FREQUENCY, order=7, use_counter=False)

################################################################################################
# Model settings:
################################################################################################

descriptor = SOAP(['H','C','N'], r_cut=3., nmax=3, lmax=2, weight=True)
kernel = C(1)*RBF(length_scale=20)
model = LSGPRModelMBKMeans(database=database, kernel=kernel, descriptor=descriptor, noise=0.01, prior=Repulsive(),
                           iteration_start_training=0, verbose=True)

################################################################################################
# Parallel Tempering Settings:
################################################################################################

sampler = ParallelTemperingSampler(temperatures=TEMPERATURES, database=database, order=6, 
                                   swap_frequency=SWAP_FREQUENCY, swap_order=9, use_counter=False)

rattle_generator = RattleGenerator(**environment.get_confinement(), environment=environment, sampler=sampler, 
                    n_rattle=N_RATTLE, rattle_amplitude=RATTLE_AMPLITUDE, dimensionality=2, order=2)

relaxer = RelaxPostprocess(model=model,
                           start_relax=1, 
                           optimizer_run_kwargs={'fmax':0.05, 'steps':200},
                           optimizer_kwargs ={'logfile': None}, 
                           optimizer=BFGS, constraints=constraints, order=2.5)


evaluator = LocalOptimizationEvaluator(calc, optimizer_kwargs={'logfile':None}, 
                                       optimizer_run_kwargs={'fmax':0.05, 'steps':1},
                                       gets={'get_key':'candidates'},
                                       constraints=environment.get_constraints(), 
                                       verbose=True, use_counter=False)

centering = CenteringPostProcess()

################################################################################################
# Let get the show running! 
################################################################################################

agox = AGOX(database, rattle_generator, relaxer, sampler, evaluator, centering, seed=run_idx)

agox.run(N_iterations=NUM_ITERATIONS)
