import matplotlib
matplotlib.use('Agg')
import argparse
import numpy as np
import os

# Common AGOXs
from ase import Atoms
from agox import AGOX

from agox.modules.environments import Environment
from agox.modules.evaluators import LocalOptimizationEvaluator
from agox.modules.generators import RattleGenerator

from agox.modules.databases.concurrent_ordered import OrderedDatabase
from agox.modules.samplers.metropolis import MetropolisSampler
from ase.constraints import FixedPlane
from agox.modules.postprocessors import WrapperPostprocess

from agox.modules.postprocessors import RelaxPostprocess
from ase.optimize import BFGS

# Local GPR model
from agox.modules.models.descriptors.soap import SOAP
from sklearn.gaussian_process.kernels import RBF, ConstantKernel as C
from agox.modules.models.local_GPR.LSGPR_MBKMeans import LSGPRModelMBKMeans
from agox.modules.models.priors.repulsive import Repulsive


################################################################################################
# Input arguments
################################################################################################

parser = argparse.ArgumentParser()
parser.add_argument('-i', '--index', type=int, default=0)
args = parser.parse_args()    
run_idx = args.index

TOTAL_WORKERS = 12
DATABASE_INDEX = run_idx // TOTAL_WORKERS
WORKER_INDEX = run_idx % TOTAL_WORKERS

TEMP = 3.
SYNC_FREQUENCY = 10
NUM_ITERATIONS = 2000
N_RATTLE = 3
RATTLE_AMPLITUDE = 3

N_Ag = [4,5,6][WORKER_INDEX % 3]
N_O = [2,3,4,5][WORKER_INDEX // 3]
print('N_Ag', N_Ag)
print('N_O', N_O)



################################################################################################
# Calculator
################################################################################################

from agox.modules.helpers.gpaw_io import GPAW_IO

calc = GPAW_IO(mode='lcao',
               xc='PBE',
               basis='dzp',
               maxiter='200',
               kpts ='(3, 3, 1)',
               poissonsolver='PoissonSolver(eps=1e-7)',
               mixer='Mixer(0.05,5,100)',
               occupations="FermiDirac(0.1)",
               gpts = "h2gpts(0.2, t.get_cell(), idiv = 8)",
               nbands='110%',
               txt='dft_log_lcao.txt', 
               modules=['from gpaw.utilities import h2gpts',
                        'from gpaw import FermiDirac',
                        'from gpaw.poisson import PoissonSolver',
                        'from gpaw import Mixer',
                    ])


################################################################################################
# Environment Settings:
################################################################################################

template = Atoms('Ag16',
                 cell=[[5.7838, -5.0101, 0.0], [5.7838, 5.0101, 0.0], [0.0, 0.0, 12.8]],
                 pbc=True)

template.set_scaled_positions([
[0.333333,0.166667,0.],
[0.708333,0.041667,0.],
[0.208333,0.541667,0.],
[0.583333,0.416667,0.],
[0.958333,0.291667,0.],
[0.458333,0.791667,0.],
[0.833333,0.666667,0.],
[0.083333,0.916667,0.],
[0.125, 0.125, 0.184481],
[0.500, 0.000, 0.184481],
[0.000, 0.500, 0.184481],
[0.375, 0.375, 0.184481],
[0.750, 0.250, 0.184481],
[0.250, 0.750, 0.184481],
[0.625, 0.625, 0.184481],
[0.875, 0.875, 0.184481],
])

confinement_corner = np.array([0, 0, 3]) - np.array([1, 0, 0])
confinement_cell = template.get_cell() + np.array([[1, -1, 0],[1, 1, 0],[0, 0, 0],])
confinement_cell[2][2] = 3


environment = Environment(template=template, symbols=f'Ag{N_Ag}O{N_O}', confinement_cell=confinement_cell, 
                        confinement_corner=confinement_corner)

################################################################################################
# Database Settings:
################################################################################################
main_directory = '/home/roenne/papers/local-model/AgxOy/data/'
sub_directory = os.path.splitext(os.path.basename(__file__))[0] + '/'
db_path = main_directory + sub_directory + 'con_db{}.db'.format(DATABASE_INDEX)
database = OrderedDatabase(filename=db_path, store_meta_information=True, write_frequency=1, worker_number=WORKER_INDEX, 
                            total_workers=TOTAL_WORKERS, sync_frequency=SYNC_FREQUENCY, order=7, use_counter=False)

################################################################################################
# Model settings:
################################################################################################

descriptor = SOAP(['O','Ag'], r_cut=5., nmax=3, lmax=2, sigma=1, weight=True, periodic=True)
kernel = C(1)*RBF(length_scale=20)
model = LSGPRModelMBKMeans(database=database, kernel=kernel, descriptor=descriptor, noise=0.01, prior=Repulsive(),
                           iteration_start_training=0, m_points=1500, verbose=True)

################################################################################################
# Parallel Tempering Settings:
################################################################################################
sampler = MetropolisSampler(temperature=TEMP, database=database, order=6,
                            gets={'get_key':'evaluated_candidates'}, sets={})

rattle_generator = RattleGenerator(**environment.get_confinement(), environment=environment, sampler=sampler, 
                                   n_rattle=N_RATTLE, rattle_amplitude=RATTLE_AMPLITUDE, order=2, c1=0.7, c2=3)

relaxer = RelaxPostprocess(model=model,
                           start_relax=1, 
                           optimizer_run_kwargs={'fmax':0.05, 'steps':100},
                           optimizer_kwargs ={'logfile': '-'}, 
                           optimizer=BFGS, constraints=environment.get_constraints(), order=2.5)


evaluator = LocalOptimizationEvaluator(calc, optimizer_kwargs={'logfile':'-'}, 
                                       optimizer_run_kwargs={'fmax':0.05, 'steps':1},
                                       gets={'get_key':'candidates'},
                                       constraints=environment.get_constraints(), 
                                       verbose=True, use_counter=False)

wrapper = WrapperPostprocess()

################################################################################################
# Let get the show running! 
################################################################################################

agox = AGOX(database, rattle_generator, relaxer, sampler, evaluator, wrapper, seed=run_idx)

agox.run(N_iterations=NUM_ITERATIONS)
