Contains the files for the Ru3N4C4 on graphene example: 

- databases.tar.gz: Contains all the AGOX database files for the runs. 
- unique_structures.traj: Contains unique structures as identified by the spectral graph algorithm. 
- lax_relaxed_structures.traj: Unique structures relaxed with lax DFT settings. 
- fine_relaxed_structures.traj: Further relaxation with fine settings. 
- spin_structures.traj: Graph filtering is run again on the fine_relaxed_structures and the ones with unique graphs are calculated while 
allowing for spin polarization. The energies reflect the spin that results in the lowest energy. 

