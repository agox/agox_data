import os
import argparse
import numpy as np
from ase import Atoms
from agox.helpers.gpaw_io import GPAW_IO

# Common AGOX
from agox import AGOX
from agox.databases import Database
from agox.environments.environment import Environment
from agox.acquisitors.LCB import LowerConfidenceBoundAcquisitor
from agox.evaluators import SinglePointEvaluator
from agox.collectors.ray_collector import  ParallelCollector

from agox.generators import  SymmetryGenerator,  SymmetryRattleGenerator

from agox.samplers.kmeans import KMeansSampler
from agox.utils.constraints.box_constraint import BoxConstraint

from agox.models.descriptors import Fingerprint
from agox.models.GPR.kernels import RBF
from agox.models.GPR.kernels import Constant as C
from agox.models.GPR import GPR
from agox.models.GPR.priors import Repulsive
from agox.models.GPR.kernels import Noise

from agox.postprocessors.ray_relax import ParallelRelaxPostprocess
from agox.postprocessors.minimum_dist import MinimumDistPostProcess
import math
# Global settings:
NUM_EPISODES = 1000
C1, C2 = 0.7, 1.25


################################################################################################
# Input arguments
################################################################################################

parser = argparse.ArgumentParser()
parser.add_argument('-i', '--index', type=int, default=0)
args = parser.parse_args()    
run_idx = args.index


SEED = run_idx

################################################################################################
# General settings:
################################################################################################
x=5.74993
template = Atoms('Sn2Pt6',
                 cell=[[x, 0, 0.0], [x/2, x*math.sqrt(3)/2, 0.0], [0.0, 0.0, 16]],
                 pbc=True)
h=2.299999
template.set_scaled_positions([
[0.,0.,h/16],
[4/6, 4/6, 0],
[0.5,0.,h/16],
[0.,0.5,h/16],
[0.5,0.5,h/16],
[1/6, 1/6, 0],
[1/6, 4/6, 0],
[4/6, 1/6, 0],
])
template=template*(2,2,1)



# Database
filename = __file__[:-3]
db_path = 'db{}.db'.format(run_idx)
database = Database(filename=db_path)

# ################################################################################################
# # Calculator
# ################################################################################################
#To run with the same settings than those provided in the paper one needs Sn setup with 4 electrons
#Here imported in the folder gpaw_setups

calc = GPAW_IO(mode='PW(400)',
    xc='PBE',
    setups="{'Pt':'10','Sn':'4e'}",
    maxiter='200',
    nbands='-15',
    kpts ='(1, 1, 1)',
    txt='dft_log.txt',
    occupations="FermiDirac(0.1)",
    gpts = "h2gpts(0.2, t.get_cell(), idiv = 8)",
    convergence="{'energy':0.005, 'density':1.0e-4, 'eigenstates':1.0e-4, 'bands':'occupied'}",
    modules=['from gpaw.utilities import h2gpts',
        'from gpaw import FermiDirac',
        'from gpaw import setup_paths',
         "setup_paths.insert(0, '/home/fbrix/gpaw_setups')"])

c=max(template.get_positions()[:,2])
confinement_cell=template.get_cell(complete=True)
confinement_cell[2,2]=4.5
confinement_corner= np.array([0,0,c+0.5])
environment = Environment(template=template, symbols='Sn11O12',
    confinement_cell=confinement_cell, confinement_corner=confinement_corner)


temp_atoms = environment.get_template()
temp_atoms += Atoms(environment.get_numbers())
descriptor = Fingerprint.from_atoms(temp_atoms)
descriptor.eta=4
# ################################################################################################
# # Model Calculator
# ################################################################################################

beta = 0.01
k0 = C(beta, (beta, beta)) * RBF(length_scale=0.55,length_scale_bounds=[1e-1,1])
k1 = C(1 - beta, (1 - beta, 1 - beta)) * RBF(length_scale=10.5,length_scale_bounds=[1,20])
kernel = C(5000, (1, 1e5)) * (k0 + k1) + Noise(0.01, (0.01, 0.1))
model_calculator = GPR(
    descriptor,
    kernel,
    database=database,
    prior=Repulsive(),
    use_ray=True,
)

################################################################################################
# Generators
################################################################################################



sym_generator= SymmetryGenerator(c1=C1,c2=C2,sym_type='slab',force_group='p6',  may_nucleate_at_several_places=True,**environment.get_confinement())

rattle_sym = SymmetryRattleGenerator(c1=0.7, c2=1.2,n_rattle=2, rattle_amplitude=1.5, let_lose_sym=0.0, **environment.get_confinement())

rattle_sym_lon = SymmetryRattleGenerator(c1=0.7, c2=1.2,n_rattle=2, rattle_amplitude=2.5,let_lose_sym=0.0,  **environment.get_confinement())





BC = BoxConstraint(confinement_cell, confinement_corner, indices=environment.get_missing_indices())

generators = [sym_generator,  rattle_sym, rattle_sym_lon]
num_samples = {0:[ 6, 40, 20]}
# ################################################################################################
# # Ensemble / Sampler / Acquisitor
# ################################################################################################

acquisitor = LowerConfidenceBoundAcquisitor(model_calculator, kappa=2)
post = ParallelRelaxPostprocess(model=acquisitor.get_acquisition_calculator(), 
                                        start_relax=8, 
                                        optimizer_run_kwargs={'fmax':0.02, 'steps':60}, 
                                        optimizer_kwargs={'logfile':None}, 
                                        fix_template=True,constraints=[BC])
sampler = KMeansSampler(descriptor=descriptor,model=model_calculator,sample_size=5,max_energy=15, database=database, verbose=False)

min_d=MinimumDistPostProcess(c1=C1, fragmented_check=False)

collector = ParallelCollector(generators=generators, num_candidates=num_samples,  sampler=sampler,  environment=environment, use_counter=True)

evaluator = SinglePointEvaluator(calc)
collector.plot_confinement()
################################################################################################
# Let get the show running! 
################################################################################################
agox = AGOX(collector, acquisitor, post, min_d  database, evaluator,  seed=SEED)

agox.run(N_iterations=NUM_EPISODES)
