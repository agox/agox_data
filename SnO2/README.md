Data for SnO2 with basin-hopping and basin-hopping with GPR: 

Scripts:
- sno2_model_bh_3_steps.py: Script using GPR model.
- sno2_std_bh_10steps.py: Standard script with 10 steps. 

Data: 
- standard_200.tar.gz: Standard basin-hopping 200 steps
- standard_50.tar.gz: Standard basin-hopping 50 steps
- standard_10.tar.gz: Standard basin-hopping 10 steps
- model_3.tar.gz: GPR basin-hopping 10 steps.
- model_10.tar.gz: GPR basin-hopping 10 steps.