import matplotlib

matplotlib.use('Agg')
import argparse
import numpy as np
from ase import Atoms
from ase.io import read
from ase.constraints import FixedPlane
from ase.optimize import BFGS
import os

# Common AGOXs
from agox import AGOX
from agox.modules.databases import Database
from agox.modules.environments import Environment
from agox.modules.collectors import StandardCollector
from agox.modules.generators import RandomGenerator, RattleGenerator
from agox.modules.postprocessors.constraints.box_constraint import BoxConstraint
from agox.modules.postprocessors import RelaxPostprocess, WrapperPostprocess
from agox.modules.samplers import MetropolisSampler
from agox.modules.evaluators import LocalOptimizationEvaluator
from agox.modules.models import ModelGPR

C1, C2 = 0.7, 3
C1s, C2s = 0.7, 3

################################################################################################
# Input arguments
################################################################################################

parser = argparse.ArgumentParser()
parser.add_argument('-i', '--index', type=int, default=0)
args = parser.parse_args()    
run_idx = args.index

################################################################################################
# Calculator
################################################################################################

from agox.modules.helpers.gpaw_io import GPAW_IO

calc = GPAW_IO(mode='lcao',
               xc='PBE',
               basis='dzp',
               maxiter='200',
               kpts ='(1, 1, 1)',
               poissonsolver='PoissonSolver(eps=1e-7)',
               mixer='Mixer(0.05,5,100)',
               convergence="{'energy':0.005, 'density':1.0e-3, 'eigenstates':1.0e-3, 'bands':'occupied'}",
               occupations="FermiDirac(0.1)",
               gpts = "h2gpts(0.2, t.get_cell(), idiv = 8)",
               nbands='110%',
               txt='dft_log_lcao.txt', 
               setups="{'Sn': '4e'}",
               modules=['from gpaw.utilities import h2gpts',
                        'from gpaw import FermiDirac',
                        'from gpaw.poisson import PoissonSolver',
                        'from gpaw import Mixer',
                        'from gpaw import setup_paths',
                        "setup_paths.insert(0, '/home/hammer/gpaw_setups')",
                    ])

################################################################################################
# General settings:
################################################################################################

template = Atoms('Sn8O16',
                 positions = np.zeros((24,3)),
                 cell=[6.9, 13, 12],
                 pbc=True)

template.set_scaled_positions([
    [  0.0000,  0.0000,  0.2500],
    [  0.0000,  0.2500,  0.2500],
    [  0.0000,  0.5000,  0.2500],
    [  0.0000,  0.7500,  0.2500],
    [  0.5000,  0.1250,  0.2360],
    [  0.5000,  0.3750,  0.2361],
    [  0.5000,  0.6250,  0.2360],
    [  0.5000,  0.8750,  0.2360],
    [  0.0000,  0.8750,  0.1420],
    [  0.6918,  0.7500,  0.2406],
    [  0.3082,  0.7500,  0.2406],
    [  0.0000,  0.6250,  0.1421],
    [  0.6918,  0.5000,  0.2407],
    [  0.3082,  0.5000,  0.2407],
    [  0.0000,  0.3750,  0.1421],
    [  0.6918,  0.2500,  0.2407],
    [  0.3082,  0.2500,  0.2407],
    [  0.0000,  0.1250,  0.1421],
    [  0.6918,  0.0000,  0.2406],
    [  0.3082,  0.0000,  0.2406],
    [  0.0000,  0.1250,  0.5 - 0.1421],
    [  0.0000,  0.3750,  0.5 - 0.1421],
    [  0.0000,  0.6250,  0.5 - 0.1421],
    [  0.0000,  0.8750,  0.5 - 0.1420],
])

environment = Environment(template=template, symbols='Sn6O6')

cell_corner = np.array([0, 0, 3])
confinement_cell = template.get_cell()
confinement_cell[2][2] = 6

relax_cell_corner = cell_corner - np.array([1, 1, 0])
relax_confinement_cell = confinement_cell + np.diag([2, 2, 0])

BC = BoxConstraint(relax_confinement_cell, relax_cell_corner, indices=environment.get_missing_indices())

start_cell_corner = cell_corner + np.array([0, 0, 0])
start_confinement_cell = confinement_cell

# Database

db_path = 'db{}.db'.format(run_idx)
database = Database(filename=db_path, write_frequency=1, order=4)

########################################################################################################################
# Model Settings
########################################################################################################################
# from agox.modules.models.gaussian_process.default_model_gofee import get_default_GPR_model
# from agox.modules.models.gaussian_process.featureCalculators_multi.angular_fingerprintFeature_cy import Angular_Fingerprint

# Rc1, binwidth1, sigma1 = 6, 0.2, 0.2
# Rc2, Nbins2, sigma2, gamma = 4, 30, 0.2, 2
# eta, use_angular = 20, True
# temp_atoms = environment.get_template()
# temp_atoms += Atoms(environment.get_numbers())

# feature_calculator = Angular_Fingerprint(temp_atoms, Rc1=Rc1, Rc2=Rc2, binwidth1=binwidth1, Nbins2=Nbins2, sigma1=sigma1, sigma2=sigma2, gamma=gamma, eta=eta, use_angular=use_angular)
# global_model, feat_calc = get_default_GPR_model(feature_calculator=feature_calculator, environment=environment, return_feature_calc=True, sigma_noise = 1e-3, use_delta_func=True, use_delta_in_training=False)
# model_calculator = ModelGPR(global_model, database=database, update_interval=1, optimize_frequency=25, optimize_loglikelyhood=True, episode_start_training=0, use_saved_features=True, order=1)

model = ModelGPR.default(environment, database)

################################################################################################
# Generator settings:
################################################################################################

start_generator = RandomGenerator(c1=C1,c2=C2,confinement_cell=start_confinement_cell, confinement_corner=start_cell_corner, may_nucleate_at_several_places=True)
rattle_generator = RattleGenerator(n_rattle=3, confinement_cell=relax_confinement_cell, confinement_corner=relax_cell_corner)

generators = [start_generator, rattle_generator]
num_candidates = {0:[3, 0],
               2:[0, 1]}

evaluator = LocalOptimizationEvaluator(calc, gets={'get_key':'candidates'}, optimizer_kwargs={'logfile':'-'}, 
    optimizer_run_kwargs={'fmax':0.05, 'steps':3}, constraints=[BC], order=3)

# ################################################################################################
# # Ensemble / Sampler / Acquisitor
# ################################################################################################

relaxer = RelaxPostprocess(model=model,
                           start_relax=10, 
                           optimizer_run_kwargs={'fmax':0.05, 'steps':100},
                           optimizer_kwargs ={'logfile':'-'}, 
                           optimizer=BFGS, constraints=[BC], order=2)

wrapper = WrapperPostprocess(order=3)
postprocessors = [relaxer, wrapper]

sampler = MetropolisSampler(temperature=1, database=database, order=0)

collector = StandardCollector(generators=generators, sampler=sampler, 
    environment=environment, num_candidates=num_candidates, order=1)

################################################################################################
# Let get the show running! 
################################################################################################

agox = AGOX(database, collector, sampler, evaluator, postprocessors, seed=run_idx)

agox.run(N_iterations=20)
